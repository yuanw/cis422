package com.example.skyline.myapplication;

import android.os.Bundle;
import android.app.Activity;
import android.support.v7.widget.Toolbar;

/**
 * Created by Skyline on 5/24/17.
 */
public class activity_help_page extends Activity{
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if(getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
}
