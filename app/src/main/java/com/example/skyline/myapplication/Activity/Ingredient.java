package com.example.skyline.myapplication.Activity;

import java.util.Comparator;

/**
 * Created by Skyline on 5/27/17.
 */

public class Ingredient implements java.io.Serializable{
    private String name; //only one variable for the name

    public Ingredient(String name) {
        this.name = name;
    } //Construction

    public String getName() {
        return name;
    } //Returns Name of the object

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return name;
    }

    public static Comparator<Ingredient> COMPARE_BY_INGREDIENT = new Comparator<Ingredient>() {
        public int compare(Ingredient one, Ingredient other) {
            return one.getName().compareTo(other.getName());
        }
    };
}
